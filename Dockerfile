FROM registry.gitlab.com/enarx/lab:latest

# Install and configure the kernel
ADD linux.tar.bz2 /
RUN cd /boot; ln -s vmlinuz* wyrcan.kernel

# Add udev rules
COPY 99-sgx.rules /etc/udev/rules.d/

# Install the AESM and PCCS daemon services
COPY aesmd-tmpfiles.conf /usr/lib/tmpfiles.d/aesmd.conf
COPY aesmd-devices.service /etc/systemd/system/aesmd-devices.service
RUN systemctl enable aesmd-devices.service
COPY pccs.service /etc/systemd/system/pccs.service
RUN systemctl enable pccs.service
COPY aesmd.service /etc/systemd/system/aesmd.service
RUN systemctl enable aesmd.service
